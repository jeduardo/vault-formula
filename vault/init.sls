{% from "vault/map.jinja" import vault with context %}
# using archive.extracted causes: 'Comment: Failed to cache https://releases.hashicorp.com/vault/0.7.0/vault_0.7.0_linux_amd64.zip: [Errno 1] _ssl.c:493: error:1409442E:SSL routines:SSL3_READ_BYTES:tlsv1 alert protocol version'
vault packages:
  pkg.installed:
    - names:
      - unzip
      - curl
      {% if vault.secure_download %}
      {% if grains['os'] == 'CentOS' or grains['os'] == 'Amazon' %}
      - gnupg2
      - perl-Digest-SHA
      {% elif grains['os'] == 'Ubuntu' %}
      - gnupg
      - libdigest-sha-perl
      {% endif %}
      {% endif %}

download vault:
  archive.extracted:
    - name: /usr/local/bin
    - source: https://releases.hashicorp.com/vault/{{ vault.version }}/vault_{{ vault.version }}_linux_amd64.zip
    - enforce_toplevel: False
    - skip_verify: True
    - force: True

#install vault:
  #  cmd.run:
    #    - name: unzip /tmp/vault_{{ vault.version }}_linux_amd64.zip -d /usr/local/bin && chmod 0755 /usr/local/bin/vault && chown root:root /usr/local/bin/vault
  #    - require:
    #      - cmd: download vault
    #      - pkg: unzip
    #    - creates: /usr/local/bin/vault

#vault set cap mlock:
#  cmd.run:
#    - name: "setcap cap_ipc_lock=+ep /usr/local/bin/vault"
#    - onchanges:
#    #      - cmd: install vault
